#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>

void HitEnemy();
void MainMenu ();

/*Player Character*/
int playerHP = 0;
int playerStr = 0;
char playerName[10];

/*Bools to close loops*/
bool playerTurn = false;
bool menuLoop = true; 

/*Enemy character*/
int enemyHP = 0;
int enemyStr = 0;

/*Ints to save menu options*/
int selection;
int battleSelection;

void HitPlayer(){
	
	printf("\nEnemy hit player and damages him with %d", enemyStr);
	playerHP = playerHP - enemyStr;
	
	printf("\n%s: %d HP remaining",playerName, playerHP);
	playerTurn = true;
}

	
void PlayerStatus() {
	
	printf("\n\nName: %s", playerName);
	printf("\nHP: %d", playerHP);
	printf("\nStr: %d \n", playerStr);
}


void EnemyStatus () {
	
	printf("\nHP: %d", enemyHP);
	printf("\nStr: %d \n", enemyStr);
	
}


void Turn(){
	
	if(playerTurn){
		
		HitEnemy();
	}
	else{
		HitPlayer();
	}
}


void HitEnemy(){
	
	battleSelection = 0;
	
	printf("\n1. Hit");
	printf("\n2. Flee\n"); 
	scanf("%d", &battleSelection);
	
	switch (battleSelection){
		case 1:
	
			printf("\n%s hit enemy and damages him with %d" , playerName , playerStr);
			enemyHP = enemyHP - playerStr;
			
			printf("\nEnemy: %d HP remaining" , enemyHP);
			playerTurn = false;
	
		break;
	
		case 2:
		
			MainMenu();
		
		break;
	
		default:
		
			printf("\nError try again"); 
		
		break;
	}
}

	
void Battle(){

	playerTurn = false;

	enemyHP = 20;
	enemyStr = rand() % 8;
	
	printf("\nA new champion appears");
	
	EnemyStatus(); 
	
	if (playerStr >= enemyStr){
		
		playerTurn = true;
		
	} 
	
	while (playerHP > 0 && enemyHP > 0 ) {
		
		Turn();
		
	}
	
	if(playerHP <=0 ){
		printf("\nYou died, GAME OVER!");
		menuLoop = false;
	}
}




void MainMenu () {
	
	selection = 0;
	
	menuLoop = true;
	
	while(menuLoop){
		
		PlayerStatus();
		
		printf("\n1. Battle");
		printf("\n2. Rest");
		printf("\n3. Exit\n");
	
		scanf("%d", &selection);
	
	switch (selection) {
		
		case 1:
		
			Battle();
		
		break;
		
		case 2:
		
			playerHP = 20;

			if (playerStr < 10) {
				
				playerStr++;
					
			}
			
		break;
		
		case 3: 
		
			menuLoop = false ;
			
		
			exit(0);
		
		default: 
			
			printf("\nError try again");		
			
		break;
		}
	}
}

int main () {

	srand(time(NULL));

	printf("Enter Your Name: ");
	scanf("%s", &playerName);
	
	playerHP = 20;
	playerStr = rand() % 10;
	
	MainMenu();
}
